package com.saphir;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtdavApplication {

	public static void main(String[] args) {
		SpringApplication.run(OtdavApplication.class, args);
	}

}
