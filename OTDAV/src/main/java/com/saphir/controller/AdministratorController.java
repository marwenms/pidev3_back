package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Administrator;
import com.saphir.entity.Role;
import com.saphir.service.AdministratorService;
import com.saphir.service.RoleService;


@RestController
@CrossOrigin("*")
public class AdministratorController {





	@Autowired
	AdministratorService administratorService;
	@Autowired
	RoleService roleService;

	@RequestMapping(value = "/administrator", method = RequestMethod.GET)
	public List<Administrator> getAll() {
		return administratorService.getAll();
	}


	@RequestMapping(value = "/administrator", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Administrator m) {
		Role r = roleService.getByRoleName("admin");
		if(r != null) {
			m.setRole(r);
		}
		administratorService.addOrUpdate(m);
		return new ResponseEntity<>("administrator is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/administrator/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		administratorService.delete(id);
		return new ResponseEntity<>("administrator is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/administrator/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Administrator p) {
		Role r = roleService.getByRoleName("admin");
		if(r != null) {
			p.setRole(r);
		}
		administratorService.addOrUpdate(p);
		return new ResponseEntity<>("administrator is updated successsfully", HttpStatus.OK);
	}





}
