package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Agent;
import com.saphir.entity.Departement;
import com.saphir.entity.Role;
import com.saphir.service.AgentService;
import com.saphir.service.RoleService;



@RestController
@CrossOrigin("*")
public class AgentController {






	@Autowired
	AgentService agentService;
	@Autowired
	RoleService roleService;

	@RequestMapping(value = "/agent", method = RequestMethod.GET)
	public List<Agent> getAll() {
		return agentService.getAll();
	}
	
	@RequestMapping(value = "/agent/{id}", method = RequestMethod.GET)
	public Agent get(@PathVariable("id") Long id) {
		return agentService.get(id);
	}


	@RequestMapping(value = "/agent", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Agent m) {
		Role r = roleService.getByRoleName("agent");
		if(r != null) {
			m.setRole(r);
		}
		agentService.addOrUpdate(m);
		return new ResponseEntity<>("agent is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/agent/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		agentService.delete(id);
		return new ResponseEntity<>("agent is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/agent/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Agent p) {
		Role r = roleService.getByRoleName("agent");
		if(r != null) {
			p.setRole(r);
		}
		agentService.addOrUpdate(p);
		return new ResponseEntity<>("agent is updated successsfully", HttpStatus.OK);
	}






}
