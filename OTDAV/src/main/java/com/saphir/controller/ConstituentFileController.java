package com.saphir.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.ConstituentFile;
import com.saphir.entity.MemberShip;
import com.saphir.entity.Role;
import com.saphir.service.ConstituentFileService;
import com.saphir.service.MemberShipService;



@RestController
@CrossOrigin("*")
public class ConstituentFileController {



	@Autowired
	ConstituentFileService constituentFileService;
	@Autowired
	MemberShipService membershipService;


	@RequestMapping(value = "/constituentFile", method = RequestMethod.GET)
	public List<ConstituentFile> getAll() {
		return constituentFileService.getAll();
	}
	
	@RequestMapping(value = "/constituentFile/bymembership/{id}", method = RequestMethod.GET)
	public List<ConstituentFile> getByMembership(@PathVariable("id") Long id) {
		
		List<ConstituentFile> roles = constituentFileService.getAll();
		return roles.stream().filter(p -> {
			return p.getMembership().getId() == id;
		}) .collect(Collectors.toList()); 
	}


	@RequestMapping(value = "/constituentFile", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody ConstituentFile m) {
		if(m.getMembership() != null) {
			MemberShip ms = membershipService.get(m.getMembership().getId());
			m.setMembership(ms);
		}
		constituentFileService.addOrUpdate(m);
		return new ResponseEntity<>("constituent File is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/constituentFile/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		constituentFileService.delete(id);
		return new ResponseEntity<>("constituent File is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/constituentFile/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody ConstituentFile p) {
		if(p.getMembership() != null) {
		MemberShip ms = membershipService.get(p.getMembership().getId());
		p.setMembership(ms);
		}
		constituentFileService.addOrUpdate(p);
		return new ResponseEntity<>("constituent File is updated successsfully", HttpStatus.OK);
	}



}
