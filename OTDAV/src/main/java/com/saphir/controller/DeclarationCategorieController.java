package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.DeclarationCategorie;
import com.saphir.service.DeclarationCategorieService;

@RestController
@CrossOrigin("*")
public class DeclarationCategorieController {

	@Autowired
	DeclarationCategorieService categorieService;

	@RequestMapping(value = "/declarationcategorie", method = RequestMethod.GET)
	public List<DeclarationCategorie> getAll() {
		return categorieService.getAll();
	}


	@RequestMapping(value = "/declarationcategorie", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody DeclarationCategorie m) {
		categorieService.addOrUpdate(m);
		return new ResponseEntity<>("categorie is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/declarationcategorie/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		categorieService.delete(id);
		return new ResponseEntity<>("categorie is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/declarationcategorie/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody DeclarationCategorie p) {
		categorieService.addOrUpdate(p);
		return new ResponseEntity<>("categorie is updated successsfully", HttpStatus.OK);
	}

}
