package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.saphir.entity.Declaration;
import com.saphir.service.DeclarationService;

@RestController
@CrossOrigin("*")
public class DeclarationController {

	@Autowired
	DeclarationService declarationService;

	@RequestMapping(value = "/declaration", method = RequestMethod.GET)
	public List<Declaration> getAll() {
		return declarationService.getAll();
	}


	@RequestMapping(value = "/declaration", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Declaration m) {
		declarationService.addOrUpdate(m);
		return new ResponseEntity<>("declaration is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/declaration/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		declarationService.delete(id);
		return new ResponseEntity<>("declaration is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/declaration/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Declaration p) {
		declarationService.addOrUpdate(p);
		return new ResponseEntity<>("declaration is updated successsfully", HttpStatus.OK);
	}
}
