package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Agent;
import com.saphir.entity.Declaration;
import com.saphir.entity.Departement;
import com.saphir.entity.MemberShip;
import com.saphir.service.DeclarationService;
import com.saphir.service.DepartementService;

@RestController
@CrossOrigin("*")
public class DepartementController {


	@Autowired
	DepartementService departementService;


	@RequestMapping(value = "/departement", method = RequestMethod.GET)
	public List<Departement> getAll() {
		return departementService.getAll();
	}
	
	@RequestMapping(value = "/departement/{id}", method = RequestMethod.GET)
	public Departement get(@PathVariable("id") Long id) {
		return departementService.get(id);
	}
	
	
	@RequestMapping(value = "/departement/{id}/agents", method = RequestMethod.GET)
	public List<Agent> agentListByDepartment(@PathVariable("id") Long id) {
		return departementService.AgentListByDepartment(id);
	}
	
	@RequestMapping(value = "/departement/agents/{id}", method = RequestMethod.GET)
	public Departement getDepartmentByAgent(@PathVariable("id") Long id) {
		return departementService.getDepartmentByAgent(id);
	}


	@RequestMapping(value = "/departement", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Departement m) {
		departementService.addOrUpdate(m);
		return new ResponseEntity<>("Department is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/departement/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		departementService.delete(id);
		return new ResponseEntity<>("department is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/departement/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Departement p ) {
		System.out.println(p);
		System.out.println(p.getAgents());
		departementService.addOrUpdate(p);
		return new ResponseEntity<>("department is updated successsfully", HttpStatus.OK);
	}

}
