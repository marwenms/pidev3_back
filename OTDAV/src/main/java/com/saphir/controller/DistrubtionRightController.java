package com.saphir.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.DeclarationCategorie;
import com.saphir.entity.Declaration;
import com.saphir.entity.DistributionRight;
import com.saphir.entity.WorkCategorie;
import com.saphir.service.DeclarationCategorieService;
import com.saphir.service.DeclarationService;
import com.saphir.service.DistrubtionRightService;

@RestController
@CrossOrigin("*")
public class DistrubtionRightController {

	@Autowired
	DistrubtionRightService DistrubtionRightService;

	@RequestMapping(value = "/DistrubtionRight", method = RequestMethod.GET)
	public List<DistributionRight> getAll() {
		return DistrubtionRightService.getAll();
	}


	@RequestMapping(value = "/DistrubtionRight", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody DistributionRight m) {
		DistrubtionRightService.addOrUpdate(m);
		return new ResponseEntity<>("DistrubtionRight is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/DistrubtionRight/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		DistrubtionRightService.delete(id);
		return new ResponseEntity<>("DistrubtionRight is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/DistrubtionRight/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody DistributionRight p) {
		DistrubtionRightService.addOrUpdate(p);
		return new ResponseEntity<>("DistrubtionRight is updated successsfully", HttpStatus.OK);
	}
	
	@RequestMapping(value="/DistrubtionRight/{id}",method=RequestMethod.GET)
	public DistributionRight getbyid(@PathVariable("id") Long id)
	{
		List<DistributionRight> roles = DistrubtionRightService.getAll();
		List<DistributionRight> rs = roles.stream().filter(p -> {
			return p.getId() == id;
		}) .collect(Collectors.toList()); 
		return rs.get(0);
	}
}
