package com.saphir.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Agent;
import com.saphir.entity.Guest;
import com.saphir.entity.Role;
import com.saphir.service.GuestService;
import com.saphir.service.RoleService;


@RestController
@CrossOrigin("*")
public class GuestController {



	@Autowired
	GuestService guestService;
	@Autowired
	RoleService roleService;


	@RequestMapping(value = "/guest", method = RequestMethod.GET)
	public List<Guest> getAll() {
		return guestService.getAll();
	}
	
	@RequestMapping(value = "/guest/byemail/{email}", method = RequestMethod.GET)
	public Guest getByEmail(@PathVariable("email") String email) {
		return guestService.getByEmail(email);
	}
	
	@RequestMapping(value = "/guest/{id}", method = RequestMethod.GET)
	public Guest get(@PathVariable("id") Long id) {
		return guestService.get(id);
	}
	
	
	


	@RequestMapping(value = "/guest", method = RequestMethod.POST , consumes="application/json")
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Guest m) {
		Role r = roleService.getByRoleName("guest");
		if(r != null) {
			m.setRole(r);
		}
		guestService.addOrUpdate(m);
		return new ResponseEntity<>("guest is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/guest/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		guestService.delete(id);
		return new ResponseEntity<>("guest is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/guest/{id}", method = RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<String> updateGuest( @RequestBody Guest p, @PathVariable("id") Long id) {
		Role r = roleService.getByRoleName("guest");
		if(r != null) {
			p.setRole(r);
		}
		guestService.addOrUpdate(p);
		return new ResponseEntity<>("guest is updated successsfully", HttpStatus.OK);
	}







}
