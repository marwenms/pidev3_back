package com.saphir.controller;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.saphir.entity.MemberShip;
import com.saphir.entity.MemberShip_Stat;
import com.saphir.entity.Role;
import com.saphir.entity.User;
import com.saphir.service.MemberShipService;
import com.saphir.service.UserService;

import models.MemberShip1;

@RestController
@CrossOrigin("*")
public class MemberShipController {

	@Autowired
	MemberShipService memberShipservice;
	@Autowired
	UserService userService;

	@RequestMapping(value = "/memberships", method = RequestMethod.GET)
	public List<MemberShip> getAll(HttpEntity<byte[]> requestEntity) throws UnsupportedEncodingException {
		String email = requestEntity.getHeaders().getFirst("email");
		if(email != null) {
			return memberShipservice.getAll().stream().filter(p -> {
				return p.getUser() != null &&  p.getUser().getEmail().equals(email);
			}) .collect(Collectors.toList()); 
		}
		return memberShipservice.getAll();
	}

	@RequestMapping(value = "/memberships/{id}", method = RequestMethod.GET)
	public MemberShip get(@PathVariable("id") Long id) {
		return memberShipservice.get(id);
	}

	@RequestMapping(value = "/memberships", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody MemberShip1 m, @RequestHeader("email") String email) {
		MemberShip ms = new MemberShip();
		ms.setId(m.id);
		ms.setName(m.name);
		if(m.stat != null) {
			ms.setStat(MemberShip_Stat.valueOf(m.stat));
		} else {
			ms.setStat(MemberShip_Stat.In_progress);
		}
		ms.setDescription(m.description);
		if(email != null) {
			User usr = userService.getByEmail(email);
			ms.setUser(usr);
		}
		memberShipservice.addOrUpdate(ms);
		return new ResponseEntity<>("Member is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/memberships/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		memberShipservice.delete(id);
		return new ResponseEntity<>("Member is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/memberships/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody MemberShip1 membership) {
		MemberShip memb = memberShipservice.get(id);
		memb.setName(membership.name);
		memb.setDescription(membership.description);
		if(membership.stat != null) {
			memb.setStat(MemberShip_Stat.valueOf(membership.stat));
		} else {
			memb.setStat(MemberShip_Stat.In_progress);
		}
		memberShipservice.addOrUpdate(memb);
		return new ResponseEntity<>("Member is updated successsfully", HttpStatus.OK);
	}

}
