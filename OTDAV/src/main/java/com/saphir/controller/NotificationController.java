package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.MemberShip_Stat;
import com.saphir.entity.Notification;
import com.saphir.service.NotificationService;



@RestController
@CrossOrigin("*")
public class NotificationController {




	@Autowired
	NotificationService notificationService;


	@RequestMapping(value = "/notification", method = RequestMethod.GET)
	public List<Notification> getAll() {
		return notificationService.getAll();
	}


	@RequestMapping(value = "/notification", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Notification m) 
	{
		System.out.println(m.getStat().toString());
		notificationService.addOrUpdate(m);
		return new ResponseEntity<>("notification is created successfully, New Notification ID = "+m.getId(), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/notification/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		notificationService.delete(id);
		return new ResponseEntity<>("notification is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/notification/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Notification p) {
		notificationService.addOrUpdate(p);
		return new ResponseEntity<>("notification is updated successsfully", HttpStatus.OK);
	}




}
