package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Payement;
import com.saphir.service.PaymentService;

@RestController
@CrossOrigin("*")
public class PaymentController {


	@Autowired
	PaymentService paymentService;


	@RequestMapping(value = "/payment", method = RequestMethod.GET)
	public List<Payement> getAll() {
		return paymentService.getAll();
	}


	@RequestMapping(value = "/payment", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Payement m) {
		paymentService.addOrUpdate(m);
		return new ResponseEntity<>("payment is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/payment/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		paymentService.delete(id);
		return new ResponseEntity<>("payment is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/payment/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Payement p) {
		paymentService.addOrUpdate(p);
		return new ResponseEntity<>("payment is updated successsfully", HttpStatus.OK);
	}








}
