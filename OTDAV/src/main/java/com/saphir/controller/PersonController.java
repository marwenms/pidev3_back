
package com.saphir.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Agent;
import com.saphir.entity.MemberShip;
import com.saphir.entity.Person;
import com.saphir.entity.Role;
import com.saphir.entity.WorkCategorie;
import com.saphir.service.PersonService;
import com.saphir.service.RoleService;

@RestController
@CrossOrigin("*")
public class PersonController {

	@Autowired
	PersonService personService;
	
	@Autowired
	RoleService roleService;

	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public List<Person> getAll() {
		return personService.getAll();
	}


	@RequestMapping(value = "/person", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Person m) {
		personService.addOrUpdate(m);
		return new ResponseEntity<>("Person is created successfully", HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/person/agent", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdateAgent(@RequestBody Agent m) {
		personService.delete(m.getId());
		//m.setId(0);
		Role r = roleService.getByRoleName("agent");
		if(r != null) {
			m.setRole(r);
		}
		
		personService.addOrUpdate(m);
		return new ResponseEntity<>("Person is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/person/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		personService.delete(id);
		return new ResponseEntity<>("Person is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/person/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Person p) {
		personService.addOrUpdate(p);
		//System.out.println(p);
		return new ResponseEntity<>("Person is updated successsfully", HttpStatus.OK);
	}
	
	@RequestMapping(value="/person/{id}",method=RequestMethod.GET)
	public Person getbyid(@PathVariable("id") Long id)
	{
		List<Person> roles = personService.getAll();
		List<Person> rs = roles.stream().filter(p -> {
			return p.getId() == id;
		}) .collect(Collectors.toList()); 
		return rs.get(0);
	}

}
