package com.saphir.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Role;
import com.saphir.service.RoleService;

@RestController
@RequestMapping("role")
@CrossOrigin("*")
public class RoleController {
	@Autowired
	RoleService roleService;
	
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<Role> getAll()
	{
		return roleService.getAll();
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public void saveOrUpdate(@RequestBody Role  m)
	{
		if(m.getId() == 1) {
			List<Role> roles = roleService.getAll();
			List<Role> rs = roles.stream().filter(p -> {
				return p.getName().equals(m.getName());
			}) .collect(Collectors.toList()); 
			m.setId(rs.get(0).getId());
		}
		roleService.addOrUpdate(m);
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public void delete(@PathVariable("id") String id)
	{
		List<Role> roles = roleService.getAll();
		List<Role> rs = roles.stream().filter(p -> {
			return p.getName().equals(id);
		}) .collect(Collectors.toList()); 
		roleService.delete(rs.get(0).getId());
	}

}
