package com.saphir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Role;
import com.saphir.entity.User;
import com.saphir.service.GuestService;
import com.saphir.service.PersonService;
import com.saphir.service.RoleService;
import com.saphir.service.UserService;

@RestController
@RequestMapping("user")
@CrossOrigin("*")
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	RoleService roleService;
	@Autowired
	PersonService personService;
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<User> getAll()
	{
		return userService.getAll();
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public void saveOrUpdate(@RequestBody User  m)
	{
		personService.delete(m.getId());
		//guestService.delete(m.getId());
		m.setId(0);
		Role r = roleService.getByRoleName("user");
		if(r != null) {
			m.setRole(r);
		}
		userService.addOrUpdate(m);
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public void delete(@PathVariable("id") Long id)
	{
		userService.delete(id);
	}
}
