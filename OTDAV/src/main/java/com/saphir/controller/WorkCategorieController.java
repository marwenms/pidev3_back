package com.saphir.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.Role;
import com.saphir.entity.WorkCategorie;
import com.saphir.service.WorkCategorieService;

@RestController
@CrossOrigin("*")
public class WorkCategorieController {

	@Autowired
	WorkCategorieService WorkCategorieService;

	@RequestMapping(value = "/workcategorie", method = RequestMethod.GET)
	public List<WorkCategorie> getAll() {
		return WorkCategorieService.getAll();
	}


	@RequestMapping(value = "/workcategorie", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody WorkCategorie m) {
		WorkCategorieService.addOrUpdate(m);
		return new ResponseEntity<>("categorie is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/workcategorie/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		WorkCategorieService.delete(id);
		return new ResponseEntity<>("categorie is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/workcategorie/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody WorkCategorie p) {
		WorkCategorieService.addOrUpdate(p);
		return new ResponseEntity<>("categorie is updated successsfully", HttpStatus.OK);
	}

	@RequestMapping(value="/workcategorie/{id}",method=RequestMethod.GET)
	public WorkCategorie getbyid(@PathVariable("id") Long id)
	{
		List<WorkCategorie> roles = WorkCategorieService.getAll();
		List<WorkCategorie> rs = roles.stream().filter(p -> {
			return p.getId() == id;
		}) .collect(Collectors.toList()); 
		return rs.get(0);
	}
}
