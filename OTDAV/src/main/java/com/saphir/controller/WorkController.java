package com.saphir.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saphir.entity.DistributionRight;
import com.saphir.entity.MemberShip;
import com.saphir.entity.Role;
import com.saphir.entity.User;
import com.saphir.entity.Work;
import com.saphir.service.DistrubtionRightService;
import com.saphir.service.MemberShipService;
import com.saphir.service.UserService;
import com.saphir.service.WorkService;

@RestController
@CrossOrigin("*")
public class WorkController {



	@Autowired
	WorkService workservice;
	@Autowired
	MemberShipService memberShipService;
	@Autowired
	UserService userservice;
	@Autowired
	DistrubtionRightService distrubtionrightservice;

	@RequestMapping(value = "/work", method = RequestMethod.GET)
	public List<Work> getAll(HttpEntity<byte[]> requestEntity) throws UnsupportedEncodingException {
		String email = requestEntity.getHeaders().getFirst("email");
		if(email != null) 
		{
			List<DistributionRight> distrubtionrightlist = new ArrayList<DistributionRight>();
			User user = userservice.getByEmail(email);
		 	List<MemberShip> Membershiplist = memberShipService.GetByUser(user);
		 	
		 	List<Work> WorkList = new ArrayList<>();
		 	if(!Membershiplist.isEmpty())
		 	{
			 	for (MemberShip memberShip : Membershiplist) 
			 	{
			 		WorkList.addAll(memberShip.getWorks());
			 	}
		 	}
		 	return WorkList;
		}
		return workservice.getAll();
	}


	@RequestMapping(value = "/work", method = RequestMethod.POST)
	public ResponseEntity<Object> saveOrUpdate(@RequestBody Work m) {
		workservice.addOrUpdate(m);
		ResponseEntity<Object> test = new ResponseEntity<>("work is created successfully, New Work ID = "+m.getId(), HttpStatus.CREATED);
		return test;
	}

	@RequestMapping(value = "/work/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
		workservice.delete(id);
		return new ResponseEntity<>("work is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/work/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Work p) {
		workservice.addOrUpdate(p);
		return new ResponseEntity<>("work is updated successsfully", HttpStatus.OK);
	}
	
	@RequestMapping(value="/work/{id}",method=RequestMethod.GET)
	public Work getbyid(@PathVariable("id") Long id)
	{
		List<Work> work = workservice.getAll();
		List<Work> rs = work.stream().filter(p -> {
			return p.getId() == id;
		}) .collect(Collectors.toList()); 
		return rs.get(0);
	}


}
