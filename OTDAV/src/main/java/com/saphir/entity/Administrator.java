package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="ADMINISTRATEUR")
@PrimaryKeyJoinColumn(name = "id")
public class Administrator extends Person implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Administrator() {
		super();
	}

	public Administrator(long id) {
		super();
		this.id = id;
	}

	
	



	
}
