package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="AGENT")
@PrimaryKeyJoinColumn(name = "id")
public class Agent  extends Person implements Serializable 
{
	@JsonBackReference(value = "department_agent" )
	@ManyToOne(cascade = CascadeType.DETACH)
   // @OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name="departementid",referencedColumnName="id")
	private Departement departement;
	
	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Agent() {
		super();
	}
	
	public Agent(long id) 
	{
		super(id);
	}
	
	public Agent(Departement departement) {
		super();
		this.departement = departement;
	}

	public Agent(String cin, String firstName, String lastName, boolean isSubscribed, Role role,
			List<MemberShip> adhesions, String Email,  Departement departement) 
	{
		super(cin, firstName, lastName, isSubscribed, role, adhesions, Email);
		this.departement = departement;
	}

	@Override
	public String toString() {
		return "Agent [departement=" + departement + "]";
	}

}
