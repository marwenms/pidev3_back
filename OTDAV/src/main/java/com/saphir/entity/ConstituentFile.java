package com.saphir.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="CONSTITUENTFILE")
public class ConstituentFile implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	
	private String path;
	
	@JsonBackReference(value = "constitutefile_membership" )
	@ManyToOne
	@JoinColumn(name = "membershipid")
	private MemberShip membership;

	
	
	public ConstituentFile(long id, String path, MemberShip membership) {
		super();
		this.id = id;
		this.path = path;
		this.membership = membership;
	}



	public ConstituentFile(String path, MemberShip membership) {
		super();
		this.path = path;
		this.membership = membership;
	}



	public ConstituentFile() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public MemberShip getMembership() {
		return membership;
	}

	public void setMembership(MemberShip membership) {
		this.membership = membership;
	}
	
	

	@Override
	public String toString() {
		return "ConstituentFile [id=" + id + ", path=" + path + ", membership=" + membership + "]";
	}

	

	
	
	
}
