package com.saphir.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="DECLARATION")
public class Declaration implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String description;
	
	private String title;
	
	@JsonBackReference(value = "declaration_membership" )
	@ManyToOne
	@JoinColumn(name = "membershipid")
	private MemberShip membership;
	
	@JsonBackReference(value = "declaration_categorie" )
	@ManyToOne
	@JoinColumn(name = "CategorieId")
	private DeclarationCategorie categorie;

	public Declaration() {
		super();
	}

	public Declaration(String description, String title) {
		super();
		this.description = description;
		this.title = title;
	}
	
	public Declaration(long id, String description, String title, MemberShip membership, DeclarationCategorie categorie) {
		super();
		this.id = id;
		this.description = description;
		this.title = title;
		this.membership = membership;
		this.categorie = categorie;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	

	public DeclarationCategorie getCategorie() {
		return categorie;
	}

	public void setCategorie(DeclarationCategorie categorie) {
		this.categorie = categorie;
	}

	public MemberShip getMembership() {
		return membership;
	}

	public void setMembership(MemberShip membership) {
		this.membership = membership;
	}

	@Override
	public String toString() {
		return "Declaration [id=" + id + ", description=" + description + ", title=" + title + ", membership="
				+ membership + ", categorie=" + categorie + "]";
	}

}
