package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="DECLARATIONCATEGORIE")
public class DeclarationCategorie implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String title;
	
	private String description;
	
	@JsonManagedReference(value = "declaration_categorie")
	@OneToMany(mappedBy="categorie", targetEntity=Declaration.class, cascade = CascadeType.ALL)
	private List<Declaration> Declarations;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Declaration> getDeclarations() {
		return Declarations;
	}

	public void setDeclarations(List<Declaration> declarations) {
		Declarations = declarations;
	}
	
	
	public DeclarationCategorie() {
		super();
	}

	public DeclarationCategorie(String title, String description, List<Declaration> declarations) {
		super();
		this.title = title;
		this.description = description;
		Declarations = declarations;
	}

	public DeclarationCategorie(long id, String title, String description, List<Declaration> declarations) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		Declarations = declarations;
	}

	@Override
	public String toString() {
		return "DeclarationCategorie [id=" + id + ", title=" + title + ", description=" + description + "]";
	}
}
