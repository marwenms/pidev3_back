package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="DEPARTEMENT")
public class Departement  implements Serializable
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String name;
	
	private String description;
	
	@JsonManagedReference(value = "department_agent")
	@OneToMany(mappedBy="departement", targetEntity=Agent.class, cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	private List<Agent> agents;

	
	
	public long getId() { return id; }

	public void setId(long id) { this.id = id; }

	public String getName() { return name; }

	public void setName(String name) { this.name = name; }

	public String getDescription() { return description; }

	public void setDescription(String description) { this.description = description; }

	public List<Agent> getAgents() { return agents; }

	public void setAgents(List<Agent> agents) {	this.agents = agents; }

	public Departement() {
		super();
	}

	public Departement(long id, String name, String description, List<Agent> agents) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.agents =agents;
	}

	public Departement(String name, String description, List<Agent> agents) {
		super();
		this.name = name;
		this.description = description;
		this.agents =agents;
	}

	@Override
	public String toString() {
		return "Departement [id=" + id + ", name=" + name + ", description=" + description + ", Agents=" + agents + "]";
	}
}
