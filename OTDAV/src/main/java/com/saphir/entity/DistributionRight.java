package com.saphir.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="DISTRIBUTIONRIGHT")
public class DistributionRight implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String description;
	
	private float Price;
	
	private Date startingDate;
	
	private Date finishingDate;

	@JsonBackReference(value = "membership_distributionRight" )
	@ManyToOne
	@JoinColumn(name = "membershipid")
	private MemberShip membership;
	
	@JsonBackReference(value = "work_distributionRight" )
	@ManyToOne
	@JoinColumn(name = "workid")
	private Work work;
	
	
	
	
	public DistributionRight(long id, String description, float price, Date startingDate, Date finishingDate,
			MemberShip membership, Work work) {
		super();
		this.id = id;
		this.description = description;
		Price = price;
		this.startingDate = startingDate;
		this.finishingDate = finishingDate;
		this.membership = membership;
		this.work = work;
	}
	
	

	public DistributionRight(long id, String description, float price, Date startingDate, Date finishingDate) {
		super();
		this.id = id;
		this.description = description;
		Price = price;
		this.startingDate = startingDate;
		this.finishingDate = finishingDate;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
	public float getPrice() {
		return Price;
	}



	public void setPrice(float price) {
		Price = price;
	}



	public Work getWork() {
		return work;
	}



	public void setWork(Work work) {
		this.work = work;
	}



	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public Date getFinishingDate() {
		return finishingDate;
	}

	public void setFinishingDate(Date finishingDate) {
		this.finishingDate = finishingDate;
	}

	public MemberShip getMembership() {
		return membership;
	}

	public void setMembership(MemberShip membership) {
		this.membership = membership;
	}

	public DistributionRight() {
		super();
	}

	public DistributionRight(long id) {
		super();
		this.id = id;
	}



	@Override
	public String toString() {
		return "DistributionRight [id=" + id + ", description=" + description + ", Price=" + Price + ", startingDate="
				+ startingDate + ", finishingDate=" + finishingDate + ", membership=" + membership + ", work=" + work
				+ "]";
	}
	
	

	
}
