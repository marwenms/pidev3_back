package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="GUEST")
@PrimaryKeyJoinColumn(name = "id")
public class Guest extends Person implements Serializable {
	

	private String ipaddress;

	
	public Guest() {
		super();
	}
	
	public Guest(long id, String cin, String firstName, String lastName, boolean isSubscribed, Role role, List<MemberShip> adhesions,String Email, String ipaddress) {
		super(id, cin, firstName,lastName,isSubscribed, role, adhesions, Email);
		this.ipaddress = ipaddress;
	}
	
	public String getIpaddress() {
		return ipaddress;
	}



	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}


	public Guest(String ipaddress) {
		super();
		this.ipaddress = ipaddress;
	}


	@Override
	public String toString() {
		return "Guest [ipaddress=" + ipaddress + "]";
	}


	
	



	

}
