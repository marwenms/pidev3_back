package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="MEMBERSHIP")
public class MemberShip implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	
	private MemberShip_Stat stat;
	
	private String description;
	
	private String name;
	
	@JsonManagedReference(value = "constitutefile_membership")
	@OneToMany(mappedBy="membership", targetEntity=ConstituentFile.class, cascade = CascadeType.ALL)
	private List<ConstituentFile> constituentFile;
	
	@JsonManagedReference(value = "declaration_membership")
	@OneToMany(mappedBy="membership", targetEntity=Declaration.class, cascade = CascadeType.ALL)
	private List<Declaration> declarations;
	
	@JsonManagedReference(value = "membership_distributionRight")
	@OneToMany(mappedBy="membership", targetEntity=DistributionRight.class, cascade = CascadeType.ALL)
	private List<DistributionRight> DistributionRights;
	
	@JsonManagedReference(value = "membership_works")
	@OneToMany(mappedBy="membership", targetEntity=Work.class, cascade = CascadeType.ALL)
	private List<Work> works;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userid")
	private User user;

	public MemberShip(long id) {
		super();
		this.id = id;
	}
	
	public MemberShip() {
		super();
	}

	public MemberShip(MemberShip_Stat stat, String description, String name, List<ConstituentFile> constituentFile,
			List<Declaration> declarations, List<DistributionRight> distributionRights, User user) {
		super();
		this.stat = stat;
		this.description = description;
		this.name = name;
		this.constituentFile = constituentFile;
		this.declarations = declarations;
		DistributionRights = distributionRights;
		this.user = user;
	}


	public MemberShip(long id, MemberShip_Stat stat, String description, String name,
			List<ConstituentFile> constituentFile, List<Declaration> declarations,
			List<DistributionRight> distributionRights, User user) {
		super();
		this.id = id;
		this.stat = stat;
		this.description = description;
		this.name = name;
		this.constituentFile = constituentFile;
		this.declarations = declarations;
		DistributionRights = distributionRights;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MemberShip_Stat getStat() {
		return stat;
	}

	public void setStat(MemberShip_Stat stat) {
		this.stat = stat;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ConstituentFile> getConstituentFile() {
		return constituentFile;
	}

	public void setConstituentFile(List<ConstituentFile> constituentFile) {
		this.constituentFile = constituentFile;
	}

	public List<Declaration> getDeclarations() {
		return declarations;
	}

	public void setDeclarations(List<Declaration> declarations) {
		this.declarations = declarations;
	}

	public List<DistributionRight> getDistributionRights() {
		return DistributionRights;
	}

	public void setDistributionRights(List<DistributionRight> distributionRights) {
		DistributionRights = distributionRights;
	}

	public User getUser() {
		return user;
	}
	
	public List<Work> getWorks() {
		return works;
	}

	public void setWorks(List<Work> works) {
		this.works = works;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "MemberShip [id=" + id + ", stat=" + stat + ", description=" + description + ", name=" + name
				+ ", constituentFile=" + constituentFile + ", declarations=" + declarations + ", DistributionRights="
				+ DistributionRights + ", user=" + user + "]";
	}
	
}
