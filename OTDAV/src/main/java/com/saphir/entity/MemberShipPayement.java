package com.saphir.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="MEMBERSHIPAYMENT")
@PrimaryKeyJoinColumn(name = "id")
public class MemberShipPayement extends Payement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;



	public MemberShipPayement(float amount, String description) {
		super(amount, description);
	}



	
	
	
	

}
