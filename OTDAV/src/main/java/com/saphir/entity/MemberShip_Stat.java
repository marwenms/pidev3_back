package com.saphir.entity;

public enum MemberShip_Stat {

	Accepted,
	Rejected,
	In_progress,
	Suspended
}
