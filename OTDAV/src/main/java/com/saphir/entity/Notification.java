package com.saphir.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="NOTIFICATION")
public class Notification  implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private Date date;
	
	private String description;
	
	private MemberShip_Stat stat;
	
	
    @OneToOne(mappedBy = "notification")
	private Work work;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public MemberShip_Stat getStat() {
		return stat;
	}

	public void setStat(MemberShip_Stat stat) {
		this.stat = stat;
	}

	
	public Notification() {
		super();
	}

	public Notification(long id) {
		super();
		this.id = id;
	}
	
	public Notification(long id,Date date,  MemberShip_Stat stat) {
		super();
		this.id = id;
		this.stat = stat;
		this.date = date;
	}
	
	public Notification(Date date,  MemberShip_Stat stat) {
		super();
		this.stat = stat;
		this.date = date;
	}
	
	public Notification(Date date, String description, MemberShip_Stat stat, Work work) {
		super();
		this.date = date;
		this.description = description;
		this.stat = stat;
		this.work = work;
	}

	public Notification(long id, Date date, String description, MemberShip_Stat stat, Work work) {
		super();
		this.id = id;
		this.date = date;
		this.description = description;
		this.stat = stat;
		this.work = work;
	}

	@Override
	public String toString() {
		return "Notification [id=" + id + ", date=" + date + ", description=" + description + ", stat=" + stat + "]";
	}
	
	
	
}
