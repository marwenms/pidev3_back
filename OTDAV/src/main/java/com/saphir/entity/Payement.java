package com.saphir.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="PAYEMENT")
@Inheritance(strategy = InheritanceType.JOINED)
@OnDelete(action = OnDeleteAction.CASCADE)
public class Payement implements Serializable{

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private float amount;
	
	private String description;
	
	

	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}




	public float getAmount() {
		return amount;
	}




	public void setAmount(float amount) {
		this.amount = amount;
	}




	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}




	public Payement(float amount, String description) {
		super();
		this.amount = amount;
		this.description = description;
	}




	public Payement(long id, float amount, String description) {
		super();
		this.id = id;
		this.amount = amount;
		this.description = description;
	}
	
	
}
