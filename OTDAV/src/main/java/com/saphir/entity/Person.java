package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="PERSON")
@Inheritance(strategy = InheritanceType.JOINED)
@OnDelete(action = OnDeleteAction.CASCADE)
@JsonIgnoreProperties("adhesions")
public class Person implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String cin;
	
	private String FirstName;
	
	private String LastName;
	
	private String Email;
		
	private boolean IsSubscribed;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
	private Role role;

    @JsonManagedReference
	@OneToMany(cascade = CascadeType.ALL,orphanRemoval=false)
	@JoinColumn(name = "userid")
	private List<MemberShip> adhesions;
	


	public Person() {
		super();
	}

	public Person(long id) {
		super();
		this.id = id;
	}

	

	

	public Person(String cin, String firstName, String lastName, boolean isSubscribed, Role role,
			List<MemberShip> adhesions, String Email) {
		super();
		this.cin = cin;
		FirstName = firstName;
		LastName = lastName;
		IsSubscribed = isSubscribed;
		this.role = role;
		this.adhesions = adhesions;
		this.Email = Email;
	}

	public Person(long id, String cin, String firstName, String lastName, boolean isSubscribed, Role role,
			List<MemberShip> adhesions, String Email) {
		super();
		this.id = id;
		this.cin = cin;
		FirstName = firstName;
		LastName = lastName;
		IsSubscribed = isSubscribed;
		this.role = role;
		this.adhesions = adhesions;
		this.Email = Email;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<MemberShip> getAdhesions() {
		return adhesions;
	}

	public void setAdhesions(List<MemberShip> adhesions) {
		this.adhesions = adhesions;
	}
	
	

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public boolean isIsSubscribed() {
		return IsSubscribed;
	}

	public void setIsSubscribed(boolean isSubscribed) {
		IsSubscribed = isSubscribed;
	}
	

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", cin=" + cin + ", FirstName=" + FirstName + ", LastName=" + LastName
				+ ", IsSubscribed=" + IsSubscribed + ", role=" + role + ", adhesions=" + adhesions + "]";
	}


	
	
	
	
	
}
