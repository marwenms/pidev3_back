package com.saphir.entity;

public enum TypeRole {
	Guest,
	Administrator,
	User,
	Agent
}
