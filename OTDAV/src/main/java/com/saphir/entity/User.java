package com.saphir.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="USER")
@Inheritance(strategy = InheritanceType.JOINED)
@OnDelete(action = OnDeleteAction.CASCADE)
public class User extends Person implements Serializable {

	
	private LegalForm formeJuridique;
	
	private String identifiantUnique;
	
	private boolean isMoral;
	
	private String raisonSocial;
	
	

	public User() {
		super();
	}

	public User(LegalForm formeJuridique, String identifiantUnique, boolean isMoral, String raisonSocial) {
		super();
		this.formeJuridique = formeJuridique;
		this.identifiantUnique = identifiantUnique;
		this.isMoral = isMoral;
		this.raisonSocial = raisonSocial;
	}

	public LegalForm getFormeJuridique() {
		return formeJuridique;
	}

	public void setFormeJuridique(LegalForm formeJuridique) {
		this.formeJuridique = formeJuridique;
	}

	public String getIdentifiantUnique() {
		return identifiantUnique;
	}

	public void setIdentifiantUnique(String identifiantUnique) {
		this.identifiantUnique = identifiantUnique;
	}

	public boolean isMoral() {
		return isMoral;
	}

	public void setMoral(boolean isMoral) {
		this.isMoral = isMoral;
	}

	public String getRaisonSocial() {
		return raisonSocial;
	}

	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}

	@Override
	public String toString() {
		return "User [formeJuridique=" + formeJuridique + ", identifiantUnique=" + identifiantUnique + ", isMoral="
				+ isMoral + ", raisonSocial=" + raisonSocial + "]";
	}
	
	
	
	
	
}
