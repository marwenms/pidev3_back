package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="WORK")
public class Work implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String name;
	
	private String description;
	
    @OneToOne
    @JoinColumn(name = "Notification_id")
    private Notification notification;
    
   
	@OneToMany(mappedBy="work")
	private List<DistributionRight> distributionRights;
	
	@JsonBackReference(value = "membership_works" )
	@ManyToOne
	@JoinColumn(name = "membershipid")
	private MemberShip membership;
	
    @JsonBackReference(value = "work_Workcategorie")
	@ManyToOne
    @JoinColumn(name="Workcategorie")
	private WorkCategorie Workcategorie;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	public List<DistributionRight> getDistributionRights() {
		return distributionRights;
	}

	public void setDistributionRights(List<DistributionRight> distributionRights) {
		distributionRights = distributionRights;
	}

	public WorkCategorie getWorkcategorie() {
		return Workcategorie;
	}

	public void setWorkcategorie(WorkCategorie workcategorie) {
		Workcategorie = workcategorie;
	}

	public Work() {
		super();
	}
	
	public Work(long id) {
		super();
		this.id = id;
	}

	public Work(String name, String description, Notification notification, List<DistributionRight> distributionRights,
			WorkCategorie workcategorie) {
		super();
		this.name = name;
		this.description = description;
		this.notification = notification;
		this.distributionRights = distributionRights;
		Workcategorie = workcategorie;
	}

	public Work(long id, String name, String description, Notification notification,
			List<DistributionRight> distributionRights, WorkCategorie workcategorie) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.notification = notification;
		this.distributionRights = distributionRights;
		Workcategorie = workcategorie;
	}

	@Override
	public String toString() {
		return "Work [id=" + id + ", name=" + name + ", description=" + description + ", notification=" + notification
				+ ", DistributionRights=" + distributionRights + ", Workcategorie=" + Workcategorie + "]";
	}
	
	
}
