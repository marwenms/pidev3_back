package com.saphir.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name="WORKCATEGORIE")
@JsonIgnoreProperties("Works")
public class WorkCategorie implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String title;
	
	private String description;
	
	
	@JsonManagedReference(value = "work_Workcategorie")
	@OneToMany(mappedBy = "Workcategorie",cascade = CascadeType.ALL,orphanRemoval=false)
	private List<Work> Works;

	
	public WorkCategorie() {
		super();
	}

	public WorkCategorie(String title, String description, List<Work> works) {
		super();
		this.title = title;
		this.description = description;
		Works = works;
	}
	
	public WorkCategorie(long id, String title, String description, List<Work> works) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		Works = works;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@JsonIgnore
	public List<Work> getWorks() {
		return Works;
	}

	public void setWorks(List<Work> works) {
		Works = works;
	}

	@Override
	public String toString() {
		return "WorkCategorie [id=" + id + ", title=" + title + ", description=" + description + ", Works=" + Works
				+ "]";
	}
	
}
