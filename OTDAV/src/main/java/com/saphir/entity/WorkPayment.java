package com.saphir.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="WORKPAYMENT")
public class WorkPayment extends Payement implements Serializable {

	
	
	
	public WorkPayment(float amount, String description) {
		super(amount, description);
}

}
