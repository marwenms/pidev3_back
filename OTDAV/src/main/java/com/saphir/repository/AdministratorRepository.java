package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Administrator;
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Long> {

}
