package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Agent;
@Repository
public interface AgentRepository extends JpaRepository<Agent, Long>{

}
