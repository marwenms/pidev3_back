package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.ConstituentFile;

@Repository
public interface ConstituentFileRepository extends JpaRepository<ConstituentFile, Long> {

}
