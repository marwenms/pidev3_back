package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.DeclarationCategorie;

@Repository
public interface DeclarationCategorieRepository extends JpaRepository<DeclarationCategorie, Long> {

}
