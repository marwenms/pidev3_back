package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Declaration;
import com.saphir.entity.DistributionRight;

@Repository
public interface DistrubtionRightRepository extends JpaRepository<DistributionRight, Long>{

}
