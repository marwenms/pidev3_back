package com.saphir.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Guest;
@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {
	
	//@Async
	//Guest getByEmail(String Email);
}
