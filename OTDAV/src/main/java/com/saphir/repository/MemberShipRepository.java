package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.MemberShip;

@Repository
public interface MemberShipRepository extends JpaRepository<MemberShip, Long> {

}
