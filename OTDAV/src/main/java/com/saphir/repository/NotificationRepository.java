package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Notification;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

}
