package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Payement;

@Repository
public interface PaymentRepositorry extends JpaRepository<Payement, Long> {

}
