package com.saphir.repository;

import java.util.List;
import java.util.concurrent.Future;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

	@Async
	List<Role> findByName(String name);
}
