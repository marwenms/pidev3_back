package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saphir.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
