package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.DeclarationCategorie;
import com.saphir.entity.WorkCategorie;

@Repository
public interface WorkCategorieRepository extends JpaRepository<WorkCategorie, Long> {

}
