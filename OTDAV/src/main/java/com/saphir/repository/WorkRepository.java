package com.saphir.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saphir.entity.Work;

@Repository
public interface WorkRepository extends JpaRepository<Work, Long>{

}
