package com.saphir.service;

import java.util.List;

import com.saphir.entity.Administrator;
import com.saphir.entity.Guest;

public interface AdministratorService {

	public List<Administrator> getAll();
	public Administrator getByEmail(String email);
	public void delete(long id);
	public void addOrUpdate(Administrator m);
}
