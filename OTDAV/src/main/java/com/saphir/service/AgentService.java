package com.saphir.service;

import java.util.List;

import com.saphir.entity.Agent;
import com.saphir.entity.Guest;

public interface AgentService {

	public List<Agent> getAll();
	public Agent get(Long id);
	public Agent getByEmail(String email);
	public void delete(Long id);
	public void addOrUpdate(Agent m);
}
