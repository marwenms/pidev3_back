package com.saphir.service;

import java.util.List;

import com.saphir.entity.ConstituentFile;

public interface ConstituentFileService {

	public List<ConstituentFile> getAll();
	public void delete(long id);
	public void addOrUpdate(ConstituentFile m);
}
