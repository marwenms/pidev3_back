package com.saphir.service;

import java.util.List;

import com.saphir.entity.DeclarationCategorie;

public interface DeclarationCategorieService {
	public List<DeclarationCategorie> getAll();
	public void delete(long id);
	public void addOrUpdate(DeclarationCategorie m);
}
