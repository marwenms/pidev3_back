package com.saphir.service;

import java.util.List;

import com.saphir.entity.ConstituentFile;
import com.saphir.entity.Declaration;

public interface DeclarationService {
	public List<Declaration> getAll();
	public void delete(long id);
	public void addOrUpdate(Declaration m);
}
