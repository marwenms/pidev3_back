package com.saphir.service;

import java.util.List;

import com.saphir.entity.Agent;
import com.saphir.entity.Declaration;
import com.saphir.entity.Departement;
import com.saphir.entity.MemberShip;

public interface DepartementService {
	public List<Departement> getAll();
	public Departement get(Long id);
	public void delete(long id);
	public void addOrUpdate(Departement m);
	public List<Agent> AgentListByDepartment(Long id);
	public Departement getDepartmentByAgent (Long id);
}
