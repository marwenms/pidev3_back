package com.saphir.service;

import java.util.List;

import com.saphir.entity.ConstituentFile;
import com.saphir.entity.Declaration;
import com.saphir.entity.DistributionRight;

public interface DistrubtionRightService {
	public List<DistributionRight> getAll();
	public void delete(long id);
	public void addOrUpdate(DistributionRight m);
}
