package com.saphir.service;

import java.util.List;

import com.saphir.entity.Agent;
import com.saphir.entity.Guest;

public interface GuestService {

	public List<Guest> getAll();
	public Guest getByEmail(String email);
	public void delete(long id);
	public void addOrUpdate(Guest m);
	public Guest get(Long id);
}
