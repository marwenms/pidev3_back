package com.saphir.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.saphir.entity.MemberShip;
import com.saphir.entity.User;

public interface MemberShipService {

	public List<MemberShip> getAll();
	public MemberShip get(Long id);
	public void delete(long id);
	public void addOrUpdate(MemberShip m);
	public List<MemberShip> GetByUser(User CurUser);
	
}
