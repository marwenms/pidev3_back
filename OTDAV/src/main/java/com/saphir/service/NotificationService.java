package com.saphir.service;

import java.util.List;

import com.saphir.entity.Notification;

public interface NotificationService {
	public List<Notification> getAll();
	public void delete(long id);
	public void addOrUpdate(Notification m);
}
