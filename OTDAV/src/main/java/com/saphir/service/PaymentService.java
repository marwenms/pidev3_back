package com.saphir.service;

import java.util.List;

import com.saphir.entity.Notification;
import com.saphir.entity.Payement;

public interface PaymentService {
	public List<Payement> getAll();
	public void delete(long id);
	public void addOrUpdate(Payement m);
}
