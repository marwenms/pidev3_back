package com.saphir.service;

import java.util.List;

import com.saphir.entity.Payement;
import com.saphir.entity.Person;

public interface PersonService {
	public List<Person> getAll();
	public void delete(long id);
	public void addOrUpdate(Person m);
}