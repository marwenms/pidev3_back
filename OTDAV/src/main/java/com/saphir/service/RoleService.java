package com.saphir.service;

import java.util.List;
import java.util.Optional;

import com.saphir.entity.Role;

public interface RoleService {


	public List<Role> getAll();
	public void delete(long id);
	public void addOrUpdate(Role m);
	public Role getByRoleName(String name);
	

}
