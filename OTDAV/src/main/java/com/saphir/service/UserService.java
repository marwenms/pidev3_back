package com.saphir.service;

import java.util.List;

import com.saphir.entity.Guest;
import com.saphir.entity.User;


public interface UserService {

	public List<User> getAll();
	public User getByEmail(String email);
	public void delete(long id);
	public void addOrUpdate(User m);
}
