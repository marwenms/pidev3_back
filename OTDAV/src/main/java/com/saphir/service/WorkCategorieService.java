package com.saphir.service;

import java.util.List;

import com.saphir.entity.DeclarationCategorie;
import com.saphir.entity.WorkCategorie;

public interface WorkCategorieService {
	public List<WorkCategorie> getAll();
	public void delete(long id);
	public void addOrUpdate(WorkCategorie m);
}
