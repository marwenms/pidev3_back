package com.saphir.service;

import java.util.List;

import com.saphir.entity.Work;

public interface WorkService {
	public List<Work> getAll();
	public void delete(long id);
	public void addOrUpdate(Work m);
}
