package com.saphir.service.serviceImp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Administrator;
import com.saphir.entity.Agent;
import com.saphir.repository.AdministratorRepository;
import com.saphir.service.AdministratorService;

@Service
public class AdministratorServiceImp implements AdministratorService{

	@Autowired
	AdministratorRepository adminRepo;
	
	@Override
	public List<Administrator> getAll() {
		return adminRepo.findAll();
	}
	
	@Override
	public Administrator getByEmail(String email) {
		List<Administrator> roles = getAll();
		List<Administrator> rs = roles.stream().filter(p -> {
			return email.equals(p.getEmail());
		}).collect(Collectors.toList()); 
		return rs.get(0);
	}
	
	@Override
	public void delete(long id) {
		adminRepo.deleteById(id);		
	}

	@Override
	public void addOrUpdate(Administrator m) {
		adminRepo.save(m);		
	}

}
