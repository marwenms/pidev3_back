package com.saphir.service.serviceImp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Agent;
import com.saphir.entity.User;
import com.saphir.repository.AgentRepository;
import com.saphir.service.AgentService;

@Service
public class AgentServiceImp implements AgentService{
	
	@Autowired
	AgentRepository agentRepo;

	@Override
	public List<Agent> getAll() {
		return agentRepo.findAll();
	}
	
	@Override
	public Agent getByEmail(String email) {
		List<Agent> roles = getAll();
		List<Agent> rs = roles.stream().filter(p -> {
			return email.equals(p.getEmail());
		}).collect(Collectors.toList()); 
		return rs.get(0);
	}

	@Override
	public void delete(Long id) {
		agentRepo.deleteById(id);		
	}

	@Override
	public void addOrUpdate(Agent m) {
		agentRepo.save(m);		
	}

	@Override
	public Agent get(Long id) {
		return agentRepo.getOne(id);
	}

}
