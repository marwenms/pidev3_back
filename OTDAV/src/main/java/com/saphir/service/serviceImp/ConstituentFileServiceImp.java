package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.ConstituentFile;
import com.saphir.repository.ConstituentFileRepository;
import com.saphir.service.ConstituentFileService;

@Service
public class ConstituentFileServiceImp implements ConstituentFileService {

	@Autowired
	ConstituentFileRepository constFRepo;
	
	
	@Override
	public List<ConstituentFile> getAll() {
		return constFRepo.findAll();
	}

	@Override
	public void delete(long id) {
		constFRepo.deleteById(id);	
	}

	@Override
	public void addOrUpdate(ConstituentFile m) {
		constFRepo.save(m);
	}

}
