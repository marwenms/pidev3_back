package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.DeclarationCategorie;
import com.saphir.repository.DeclarationCategorieRepository;
import com.saphir.service.DeclarationCategorieService;

@Service
public class DeclarationCategorieServiceImp implements DeclarationCategorieService{
	
	@Autowired
	DeclarationCategorieRepository DeclarationCategorieRep;

	@Override
	public List<DeclarationCategorie> getAll() {
		return DeclarationCategorieRep.findAll();
	}

	@Override
	public void delete(long id) {
		DeclarationCategorieRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(DeclarationCategorie m) {
		DeclarationCategorieRep.save(m);
	}

}
