package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Declaration;
import com.saphir.repository.DecalarationRepository;
import com.saphir.service.DeclarationService;

@Service
public class DeclarationServiceImp implements DeclarationService{

	@Autowired
	DecalarationRepository decRep;
	
	@Override
	public List<Declaration> getAll() {
		return decRep.findAll();
	}

	@Override
	public void delete(long id) {
		decRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(Declaration m) {
		decRep.save(m);
	}

}
