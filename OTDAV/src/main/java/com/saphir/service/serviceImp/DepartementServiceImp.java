package com.saphir.service.serviceImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Agent;
import com.saphir.entity.Departement;
import com.saphir.entity.MemberShip;
import com.saphir.entity.Role;
import com.saphir.repository.AgentRepository;
import com.saphir.repository.DepartementRepository;
import com.saphir.service.DepartementService;

@Service
public class DepartementServiceImp implements DepartementService {

	@Autowired
	DepartementRepository depRepo;
	@Autowired
	AgentRepository agentRepo;
	
	@Override
	public List<Departement> getAll() {
		return depRepo.findAll();
	}

	
	
	@Override
	public void delete(long id) {
		depRepo.deleteById(id);		
	}

	@Override
	public void addOrUpdate(Departement m) {
		depRepo.save(m);
	}



	@Override
	public Departement get(Long id) {
		return depRepo.getOne(id);
	}



	@Override
	public List<Agent> AgentListByDepartment(Long id) {
		// TODO Auto-generated method stub
		return depRepo.getOne(id).getAgents();
	}



	@Override
	public Departement getDepartmentByAgent(Long id) {
		Agent ag = agentRepo.getOne(id);
		System.out.println(ag.getDepartement().getId());		
		// TODO Auto-generated method stub		
//		List<Departement> roles = depRepo.findAll();
//		List<Departement> rs = roles.stream().filter(d -> {
//			List <Agent> ag= d.getAgents();
//			ag.stream().filter(a -> {
//				
//			})
//			//return d.getAgents()
//		}) .collect(Collectors.toList()); 
//		return rs.get(0);
		ag.getDepartement().setAgents(new ArrayList<>());
		System.out.println(ag.getDepartement().getAgents().size());
		return ag.getDepartement();
	}


	
}
