package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.DistributionRight;
import com.saphir.repository.DistrubtionRightRepository;
import com.saphir.service.DeclarationService;
import com.saphir.service.DistrubtionRightService;

@Service
public class DistrubionRightServiceImp implements DistrubtionRightService{

	@Autowired
	DistrubtionRightRepository decRep;
	
	@Override
	public List<DistributionRight> getAll() {
		return decRep.findAll();
	}

	@Override
	public void delete(long id) {
		decRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(DistributionRight m) {
		decRep.save(m);
	}

}
