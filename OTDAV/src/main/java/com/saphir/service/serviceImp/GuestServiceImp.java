package com.saphir.service.serviceImp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Guest;
import com.saphir.entity.Role;
import com.saphir.repository.GuestRepository;
import com.saphir.service.GuestService;

@Service
public class GuestServiceImp implements GuestService {

	@Autowired
	GuestRepository guestRep;
	
	@Override
	public List<Guest> getAll() {
		return guestRep.findAll();
	}
	
	@Override
	public Guest getByEmail(String email) {
		List<Guest> roles = getAll();
		List<Guest> rs = roles.stream().filter(p -> {
			return email.equals(p.getEmail());
		}).collect(Collectors.toList()); 
		return rs.get(0);
	}

	@Override
	public void delete(long id) {
		guestRep.deleteById(id);
		}

	@Override
	public void addOrUpdate(Guest m) {
		guestRep.save(m);
	}

	@Override
	public Guest get(Long id) {
		return guestRep.getOne(id);
	}

}
