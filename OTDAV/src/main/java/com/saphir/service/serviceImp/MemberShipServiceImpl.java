package com.saphir.service.serviceImp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.MemberShip;
import com.saphir.entity.Role;
import com.saphir.entity.User;
import com.saphir.repository.MemberShipRepository;
import com.saphir.service.MemberShipService;

@Service
public class MemberShipServiceImpl implements MemberShipService {

	@Autowired
	MemberShipRepository memberShipRep;
	
	@Override
	public List<MemberShip> getAll() {
		return memberShipRep.findAll();
	}
	
	@Override
	public MemberShip get(Long id) {
		return memberShipRep.getOne(id);
	}

	@Override
	public void delete(long id) {
		memberShipRep.deleteById(id);		
	}

	@Override
	public void addOrUpdate(MemberShip m) {
		memberShipRep.save(m);
	}

	@Override
	public List<MemberShip> GetByUser(User CurUser) 
	{
		List<MemberShip> roles = getAll();
		List<MemberShip> rs = roles.stream().filter(p -> {
			return p.getUser().getEmail().equals(CurUser.getEmail());
		}) .collect(Collectors.toList()); 
		return rs;
	}

}
