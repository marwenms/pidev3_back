package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Notification;
import com.saphir.repository.NotificationRepository;
import com.saphir.service.NotificationService;

@Service
public class NotificationServiceImp implements NotificationService {

	
	@Autowired
	NotificationRepository notificationRepo;
	
	
	@Override
	public List<Notification> getAll() {
		return notificationRepo.findAll();
	}

	@Override
	public void delete(long id) {
		notificationRepo.deleteById(id);		
	}

	@Override
	public void addOrUpdate(Notification m) {
		notificationRepo.save(m);		
	}

}
