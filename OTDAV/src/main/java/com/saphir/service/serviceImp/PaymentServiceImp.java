package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Payement;
import com.saphir.repository.PaymentRepositorry;
import com.saphir.service.PaymentService;

@Service
public class PaymentServiceImp implements PaymentService{

	
	@Autowired
	PaymentRepositorry paymentRep;
	
	@Override
	public List<Payement> getAll() {
		return paymentRep.findAll();
	}

	@Override
	public void delete(long id) {
		paymentRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(Payement m) {
		paymentRep.save(m);	
	}

}
