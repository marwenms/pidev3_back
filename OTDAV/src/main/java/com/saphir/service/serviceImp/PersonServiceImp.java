package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Person;
import com.saphir.repository.PersonRepository;
import com.saphir.service.PersonService;

@Service
public class PersonServiceImp implements PersonService{

	@Autowired
	PersonRepository personRepo;
	
	
	@Override
	public List<Person> getAll() {
		return personRepo.findAll();
	}

	@Override
	public void delete(long id) {
		personRepo.deleteById(id);		
	}

	@Override
	public void addOrUpdate(Person m) {
		personRepo.save(m);		
	}

}
