package com.saphir.service.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import com.saphir.entity.Role;
import com.saphir.repository.RoleRepository;
import com.saphir.service.RoleService;

@Service
public class RoleServiceImp implements RoleService{
	
	@Autowired
	RoleRepository roleRep;

	@Override
	public List<Role> getAll() {
		return roleRep.findAll();
	}

	@Override
	public void delete(long id) {
		roleRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(Role m) {
		roleRep.save(m);
	}
	@Override
	public Role getByRoleName(String name){
		//Role r =  new Role();
		//r.setName(name);
		//ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
		//Example<Role> example = Example.of(r, caseInsensitiveExampleMatcher);
		return roleRep.findByName(name).get(0);
		//roleRep.findOne(example)
	}


}
