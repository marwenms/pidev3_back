package com.saphir.service.serviceImp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saphir.entity.Guest;
import com.saphir.entity.User;
import com.saphir.repository.UserRepository;
import com.saphir.service.UserService;

@Service
public class UserServiceImp implements UserService {
	
	@Autowired
	UserRepository userRep;

	@Override
	public List<User> getAll() {
		return userRep.findAll();
	}
	
	@Override
	public User getByEmail(String email) {
		List<User> roles = getAll();
		List<User> rs = roles.stream().filter(p -> {
			return email.equals(p.getEmail());
		}).collect(Collectors.toList()); 
		return rs.get(0);
	}


	@Override
	public void delete(long id) {
		userRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(User m) {
		userRep.save(m);
	}

}
