package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.saphir.entity.WorkCategorie;
import com.saphir.repository.WorkCategorieRepository;
import com.saphir.service.WorkCategorieService;

@Service
public class WorkCategorieServiceImp implements WorkCategorieService{
	
	@Autowired
	WorkCategorieRepository WorkCategorieRep;

	@Override
	public List<WorkCategorie> getAll() {
		return WorkCategorieRep.findAll();
	}

	@Override
	public void delete(long id) {
		WorkCategorieRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(WorkCategorie m) {
		WorkCategorieRep.save(m);
	}

}
