package com.saphir.service.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.saphir.entity.Work;
import com.saphir.repository.WorkRepository;
import com.saphir.service.WorkService;

@Service
public class WorkServiceImp implements WorkService{

	
	@Autowired
	WorkRepository userRep;

	@Override
	public List<Work> getAll() {
		return userRep.findAll();
	}

	@Override
	public void delete(long id) {
		userRep.deleteById(id);
	}

	@Override
	public void addOrUpdate(Work m) {
		userRep.save(m);
	}


}
